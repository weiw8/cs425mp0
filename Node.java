import java.net.Socket;
import java.util.Scanner;
import java.io.PrintStream;
import java.io.IOException;

public class Node {
    private String name;
    private String host;
    private int port;
    private final int runtime = 120000;//(unit:ms)
    public Node(String name, String host, int port) {
        this.name = name;
        this.host = host;
        this.port = port;
    }
    public void send()
    throws IOException
    {    
    Socket soc = new Socket(host,port);
        PrintStream ps = new PrintStream(soc.getOutputStream());
        ps.println(name);
        Scanner s = new Scanner(System.in);
        long starttime = System.currentTimeMillis();
        while (s.hasNextLine() && System.currentTimeMillis() - starttime <= runtime)
         {
           ps.println(s.nextLine());
        }
        s.close();
        ps.close();
        soc.close();  
    }
    public static void main(String args[]) 
    throws IOException
    {
	    if (args.length != 3) {
		    System.out.println("usage: Node <name> <host> <port>");
		    return;
	    }
        Node test = new Node(args[0],args[1],Integer.parseInt(args[2]));
        test.send();
    }
}
