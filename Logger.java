import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.LocalDateTime;
class LogFromNode implements Runnable {
  private Socket s;
  private String name;
  private BufferedReader br;
  private ArrayList<String> personallog;
  public LogFromNode(Socket s, double connecttime, ArrayList<String> personallog) 
  {
    this.s = s;
    this.personallog = personallog;
    try {
    br = new BufferedReader(new InputStreamReader(s.getInputStream()));
    name = br.readLine();
    System.out.println(String.format("%.7f",connecttime) + " - " + name + " connected");
    } 
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  public void run() 
  {
    String exceptinfo = "";
    try {
    String event ="";
    while ((event = br.readLine())!=null) {
      long gettime =  System.currentTimeMillis()/1000*1000000000 + LocalDateTime.now().getLong(ChronoField.NANO_OF_SECOND);
      String[] eve = event.split(" ");
      personallog.add(gettime + ","+event.length()+","+eve[0]);//unit.ns
      System.out.println(eve[0] + " " + name + " "+eve[1]);
    }
    }
    catch(Exception e) {
    e.printStackTrace();
    exceptinfo = "unexpected disconnected";
    }
    double stopsec = (double) System.currentTimeMillis()/1000;
    System.out.println(String.format("%.7f",stopsec) + " - " + name + " disconnected"+exceptinfo);
    try {
    br.close();
    s.close();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  
}

public class Logger {
    private ServerSocket soc;
    private ExecutorService pool;
    private final int maxnodenum = 8;
    private File f;
    private ArrayList<ArrayList<String>> serverlog;
    public Logger(int port)
    throws IOException 
    {
      soc = new ServerSocket(port);
      pool = Executors.newFixedThreadPool(maxnodenum);
      serverlog = new ArrayList<>();
      Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
      public void run()
      {
	      try{
	      finalizelog();}
	      catch (Exception e) {
		      e.printStackTrace();
	      }
      }
      }));
    }
    private void finalizelog() 
    throws Exception
    {
	soc.close();    
        pool.shutdown();
        File centralizedlog = new File("centrallog.txt");
        centralizedlog.createNewFile();
        BufferedWriter out = new BufferedWriter(new FileWriter(centralizedlog));
        for (ArrayList<String> singlelog:serverlog) {
          for (String s:singlelog) {
            out.write(s+"\r\n");
	    }}
	out.flush();
	out.close();

    }
    public void startListen() 
    throws IOException
    {
       
        while (true) {
          Socket s = soc.accept();
          double connecttime = (double)System.currentTimeMillis()/1000;
          ArrayList<String> nodelog = new ArrayList<>();
          serverlog.add(nodelog);
          pool.submit(new LogFromNode(s,connecttime,nodelog));
        }
    }
    public static void main(String[] args)
    throws IOException
     {
        Logger log = new Logger(Integer.parseInt(args[0]));
        log.startListen();
    }
}
